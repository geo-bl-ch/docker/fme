FROM debian:8-slim

LABEL maintainer="Karsten Deininger <karsten.deininger@bl.ch>"

ENV LANG="C.UTF-8" \
    LANGUAGE="C.UTF-8" \
    LC_ALL="C.UTF-8" \
    PATH="${PATH}:/opt/fme-desktop-2017/bin" \
    LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:/opt/fme-desktop-2017/fmecore"

RUN useradd --uid 1001 --gid 0 --shell /bin/bash --home-dir /data --create-home fme && \
    apt-get update && \
    apt-get purge -y libcurl3-gnutls || true && \
    apt-get autoremove -y && \
    apt-get upgrade -y && \
    apt-get install -y wget libcurl3 curl zip unzip iputils-ping gdebi-core && \
    wget https://downloads.safe.com/fme/2017/fme-desktop-2017_2017.1.2.1.17725~ubuntu.14.04_amd64.deb && \
    mkdir -p /usr/share/man/man1 && \
    gdebi -n fme-desktop-2017_2017.1.2.1.17725~ubuntu.14.04_amd64.deb && \
    apt-get purge -y wget gdebi-core && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /usr/share/man && \
    rm -f fme-desktop-2017_2017.1.2.1.17725~ubuntu.14.04_amd64.deb && \
    chgrp -R 0 /etc/passwd && \
    chmod -R g=u /etc/passwd && \
    chgrp -R 0 /data && \
    chmod -R g=u /data

RUN apt-get update && \
    apt-get install -y wget unzip libaio1 && \
    wget https://download.oracle.com/otn_software/linux/instantclient/19600/instantclient-basiclite-linux.x64-19.6.0.0.0dbru.zip && \
    unzip instantclient-basiclite-linux.x64-19.6.0.0.0dbru.zip && \
    rm -f instantclient-basiclite-linux.x64-19.6.0.0.0dbru.zip && \
    mkdir /opt/oracle && \
    mv instantclient_19_6 /opt/oracle/ && \
    mkdir /opt/oracle/instantclient_19_6/lib && \
    ln -s /opt/oracle/instantclient_19_6/* /opt/oracle/instantclient_19_6/lib/ && \
    apt-get purge -y wget unzip && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /usr/share/man

ENV LD_LIBRARY_PATH="/opt/oracle/instantclient_19_6:${LD_LIBRARY_PATH}" \
    ORACLE_HOME=/opt/oracle/instantclient_19_6

WORKDIR /data

USER 1001

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["/bin/bash"]
