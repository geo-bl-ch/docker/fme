# Dockerized FME

This image allows FME to be run in a Debian container without GUI.

## Usage

To run FME, you have to provide a valid license. Please refer to the [Licensing from the Linux Command Line](https://docs.safe.com/fme/html/FME_Desktop_Documentation/FME_Desktop_Admin_Guide/FMEInstallation/Licensing-Linux.htm) documentation for the available licensing options. The values can be provided by the following environment variables:

- `LICENSE_FLOATING` (needs also `LICENSE_TYPE`)
- `LICENSE_FILE`
- `LICENSE_SERIAL`
- `LICENSE_TRIAL`

The default working directory is `/data`. You can extend this image an copy your files there or mount your project folder to this location.

## Example

```bash
docker run -e LICENSE_FLOATING=my.license.server -v /path/to/workbench:/data registry.gitlab.com/geo-bl-ch/docker/fme:latest fmw my_workbench.fmw --PARAM1 foo --PARAM2 bar
```
